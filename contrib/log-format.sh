#!/bin/bash

# A very simple script to format freedink trace files
# 
# Copyright (C) 2022 CRTS
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

declare in="$1"
declare out="$2"

usage() {
	cat <<-EOF
	Usage: $0 logfile [outputfile]
	EOF
}

if [[ ! -f "$in" ]];then
	echo "File not found: $1" >&2
	usage
	exit 1
fi

if [[ -z "$out" ]];then
	out=$(basename "$in").formatted
fi

indent=0

while read level tag line;do
	if [[ "$tag" == "Enter:" ]];then
		printf "%*s" $indent
		echo "$tag $line"
		(( indent++ ))
	elif [[ "$tag" == "Exit:" ]];then
		((indent--))
		printf "%*s" $indent
		echo "$tag $line"
	else
		printf "%*s" $indent
		echo "$tag $line"
	fi
done < "$in" > "$out"


# Friulian translation of freedink.
# Copyright (C) 2017 THE FREEDINK'S COPYRIGHT HOLDER
# This file is distributed under the same license as the freedink package.
# Fabio Tomat <f.t.public@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: freedink 108.2\n"
"Report-Msgid-Bugs-To: bug-freedink@gnu.org\n"
"POT-Creation-Date: 2014-05-27 20:36+0000\n"
"PO-Revision-Date: 2017-06-01 09:19+0200\n"
"Last-Translator: Fabio Tomat <f.t.public@gmail.com>\n"
"Language-Team: Friulian <f.t.public@gmail.com>\n"
"Language: fur\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.12\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/init.c:108
#, c-format
msgid "Usage: %s [OPTIONS]...\n"
msgstr "Ûs: %s [OPZIONS]...\n"

#: src/init.c:110
#, c-format
msgid "Starts the Dink Smallwood game or one of its D-Mods."
msgstr "Al tache il zûc Dink Smallwood o un dai siei D-Mods."

#: src/init.c:112
#, c-format
msgid "Edit the Dink Smallwood game or one of its D-Mods."
msgstr "Modifiche il zûc Dink Smallwood o un dai siei D-Mods."

#: src/init.c:115
#, c-format
msgid "  -h, --help            Display this help screen\n"
msgstr "  -h, --help            Mostre cheste videade di jutori\n"

#: src/init.c:116
#, c-format
msgid "  -v, --version         Display the version\n"
msgstr "  -v, --version         Mostre la version\n"

#: src/init.c:118
#, c-format
msgid "  -g, --game <dir>      Specify a DMod directory\n"
msgstr "  -g, --game <dir>      Specifiche une cartele DMod\n"

#: src/init.c:119
#, c-format
msgid "  -r, --refdir <dir>    Specify base directory for dink/graphics, D-Mods, etc.\n"
msgstr "  -r, --refdir <dir>    Specifiche la cartele di base par dink/graphics, D-Mods, e vie indenant.\n"

#: src/init.c:121
#, c-format
msgid "  -d, --debug           Explain what is being done\n"
msgstr "  -d, --debug           Spieghe ce che al sta fasint\n"

#: src/init.c:122
#, c-format
msgid "  -i, --noini           Do not attempt to write dinksmallwood.ini\n"
msgstr "  -i, --noini           No sta cirî di scrivi dinksmallwood.ini\n"

#: src/init.c:123
#, c-format
msgid "  -j, --nojoy           Do not attempt to use joystick\n"
msgstr "  -j, --nojoy           No sta cirî di doprâ il joystick\n"

#: src/init.c:124
#, c-format
msgid "  -s, --nosound         Do not play sound\n"
msgstr "  -s, --nosound         No sta riprodusi suns\n"

#: src/init.c:125
#, c-format
msgid "  -t, --truecolor       Allow more colours (for recent D-Mod graphics)\n"
msgstr "  -t, --truecolor       Permet plui colôrs (pe grafiche D-Mod resinte)\n"

#: src/init.c:126
#, c-format
msgid "  -w, --window          Use windowed mode instead of screen mode\n"
msgstr "  -w, --window          Dopre modalitât a barcon invezit de modalitât schermi\n"

#: src/init.c:127
#, c-format
msgid "  -7, --v1.07           Enable v1.07 compatibility mode\n"
msgstr "  -7, --v1.07           Abilite modalitât di compatibilitât v1.07\n"

#. TRANSLATORS: The placeholder indicates the bug-reporting address
#. for this package.  Please add _another line_ saying "Report
#. translation bugs to <...>\n" with the address for translation
#. bugs (typically your translation team's web or email
#. address).
#: src/init.c:137
#, c-format
msgid "Report bugs to %s.\n"
msgstr "Segnale erôrs a %s.\n"

#: src/init.c:279
#, c-format
msgid "Note: -nomovie is accepted for compatiblity, but has no effect.\n"
msgstr "Note: -nomovie e je acetade pe compatibilitât, ma nol à efiets.\n"

#: src/freedink.c:2679
msgid "`$I don't see anything here."
msgstr "`$No viôt nuie achì."

#: src/freedink.c:2680
msgid "`$Huh?"
msgstr "`$Eh?"

#: src/freedink.c:2681
msgid "`$I'm fairly sure I can't talk to or use that."
msgstr "`$O soi vonde sigûr di no rivâ a doprâlu o cjacarâi."

#: src/freedink.c:2682
msgid "`$What?"
msgstr "`$Ce?"

#: src/freedink.c:2683
msgid "`$I'm bored."
msgstr "`$O soi stuf."

#: src/freedink.c:2684
msgid "`$Not much happening here."
msgstr "`$Al capite ben pôc achì."

#: src/freedink.c:2792
msgid "`$I don't know any magic."
msgstr "`$No cognòs nissune magjie."

#: src/freedink.c:2793
msgid "`$I'm no wizard!"
msgstr "`$No soi un mâc!"

#: src/freedink.c:2794
msgid "`$I need to learn magic before trying this."
msgstr "`$O scugni imparâ la magjie prime di provâ chest."

#: src/freedink.c:2795
msgid "`$I'm gesturing wildly to no avail!"
msgstr "`$O mandi segnâi a plen ma dibant!"

#: src/freedink.c:2796
msgid "`$Nothing happened."
msgstr "`$Nol è sucedût nuie."

#: src/freedink.c:2797
msgid "`$Hocus pocus!"
msgstr "`$Abracadabra!"

#: src/dinkc.c:687
msgid "Attack"
msgstr "Atache"

#: src/dinkc.c:688
msgid "Talk/Examine"
msgstr "Cjacare/Esamine"

#: src/dinkc.c:689
msgid "Magic"
msgstr "Magjie"

#: src/dinkc.c:690
msgid "Item Screen"
msgstr "Videade dal ogjet"

#: src/dinkc.c:691
msgid "Main Menu"
msgstr "Menu principâl"

#: src/dinkc.c:692
msgid "Map"
msgstr "Mape"

#: src/dinkc.c:693 src/dinkc.c:694 src/dinkc.c:695 src/dinkc.c:696
msgid "Unused"
msgstr "No doprât"

#: src/dinkc.c:697
msgid "Down"
msgstr "Jù"

#: src/dinkc.c:698
msgid "Left"
msgstr "Çampe"

#: src/dinkc.c:699
msgid "Right"
msgstr "Drete"

#: src/dinkc.c:700
msgid "Up"
msgstr "Sù"

#: src/dinkc.c:701
msgid "Error: not mapped"
msgstr "Erôr: no in mape"

#: src/dinkc.c:712
#, c-format
msgid "Slot %d - %d:%02d - %s"
msgstr "Spazi %d - %d:%02d - %s"

#: src/dinkc.c:715
#, c-format
msgid "Slot %d - Empty"
msgstr "Spazi %d - Vueit"
